<?php include 'layout/_header.php' ?>

<form method="post">
<?php
  if (isset($error)) {
      echo "<p class='error'>$error</p>";
  }
?>

  <h4>New link</h4>
  <p>
    <label for="url">URL<span class="required">*</label>
    <br>
    <input required type="url" id="url" name="url" placeholder="i.e. http://jazz.local" value="<?= strip_tags($url) ?>"/>
  </p>
  <p>
    <label for="slug">Slug</label>
    <br>
    <input type="text" id="slug" name="slug" placeholder="i.e. jazz" value="<?= strip_tags($slug) ?>"/>
  </p>
  <input type="submit" value="Create"/>
</form>

<?php include 'layout/_footer.php' ?>
