<?php include 'layout/_header.php' ?>

<h4><?= $link->slug ?></h4>
<p>
  <label for="link">Link</label>
  <br>
  <input id="link" value="<?= $link->local_url() ?>"  onclick="this.select();"/>
</p>
<p>
  <label>URL</label>
  <br>
  <a target="_blank" href="<?= strip_tags($link->url) ?>"><?= strip_tags($link->url) ?></a>
</p>

<?php include 'layout/_footer.php' ?>
