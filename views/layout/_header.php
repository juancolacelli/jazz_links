<!DOCTYPE html>
<html>
  <head>
    <title>Jazz links</title>
		<link rel="stylesheet" type="text/css" href="/style.css">
  </head>
  <body>
    <header>
      <pre>
   _
  (_)
   _  __ _ ________
  | |/ _` |_  /_  /
  | | (_| |/ / / /
  | |\__,_/___/___|
 _/ |
|__/  <a href="/">home</a> <a target="_blank" href="https://gitlab.com/juancolacelli/jazz_links">fork me!</a>
      </pre>
    </header>
