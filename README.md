# Jazz links
As you seen on [jazz](http://links.jazz.local).

## Requirements
- Nginx
- MySQL / MariaDB
- PHP
  - FPM
  - MySQLi

## Config
Rename config.php.sample as config.php and change the configuration parameters values.

## Nginx
Copy and paste the following rewrite rule in your nginx.conf.
```
location / {
    try_files $uri $uri/ @router;
}

location @router {
    rewrite ^/links/(.*)/?$ /index.php?action=show&id=$1 last;
    rewrite ^/(.*)/?$ /index.php?action=redirect&id=$1 last;
}
```

## Database
Run the following command in your mysql.
*Remember to replace jazz_links_user and jazz_links_password with username and password.*

```mysql
CREATE DATABASE jazz_links;
GRANT ALL ON jazz_links.* TO 'jazz_links_user'@'localhost' IDENTIFIED BY 'jazz_links_password';
USE jazz_links;
CREATE TABLE links(id INT NOT NULL AUTO_INCREMENT, url VARCHAR(300) NOT NULL, slug VARCHAR(300) NOT NULL, created_at DATETIME NOT NULL DEFAULT NOW(), PRIMARY KEY (id));
```
