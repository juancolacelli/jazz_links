<?php
  class Link {
    public $id;
    public $url;
    public $slug;
    public $local_url;

    public $errors = [];

    public static function find($link_id) : ?Link {
      $link_id = Database::instance()->escape($link_id);

      $query = "SELECT id, url, slug FROM links WHERE slug = '$link_id' LIMIT 1";
      $query_result = Database::instance()->query($query);

      $link = null;

      // Found!
      if ($query_result->num_rows > 0) {
        $row = $query_result->fetch_row();

        $link = new Link();
        $link->id = $row[0];
        $link->url = $row[1];
        $link->slug = $row[2];
      }

      return $link;
    }

    public static function all($limit = 5) : ?array {
      $query = "SELECT id, url, slug FROM links ORDER BY id DESC LIMIT $limit";
      $query_result = Database::instance()->query($query);

      $links = [];

      // Found!
      if ($query_result->num_rows > 0) {
        while ($row = $query_result->fetch_row()) {
          $link = new Link();
          $link->id = $row[0];
          $link->url = $row[1];
          $link->slug = $row[2];

          array_push($links, $link);
        }
      }

      return $links;
    }

    function is_slug_available() : bool {
      $query_result = Database::instance()->query("SELECT slug FROM links WHERE slug = '$this->slug'");
      return $query_result->num_rows == 0;
    }

    function is_valid() : bool {
      $this->url = trim($this->url);
      $this->url = filter_var($this->url, FILTER_VALIDATE_URL);
      $this->url = Database::instance()->escape($this->url);

      $this->slug = strtolower(trim($this->slug));
      if ($this->slug === '') $this->slug = time();
      $this->slug = Database::instance()->escape($this->slug);

      $validations = [
        'url' => [
          $this->url != ''
        ],

        'slug' => [
          $this->slug != '',
          preg_match('/^[a-z0-9-]+$/', $this->slug) == 1,
          $this->is_slug_available()
        ]
      ];

      foreach ($validations as $attribute => $attr_validations) {
        foreach ($attr_validations as $validation) {
          if (!in_array($attribute, $this->errors) && !$validation) {
            array_push($this->errors, $attribute);
          }
        }
      }

      return sizeof($this->errors) == 0;
    }

    public function save() : bool {
      $valid = $this->is_valid();

      if ($valid) {
        return Database::instance()->query("INSERT INTO links(url, slug) VALUES('$this->url', '$this->slug')");
      } else {
        return $valid;
      }
    }


    public function local_url() : string {
      return $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'] . "/$this->slug";
    }
  }
