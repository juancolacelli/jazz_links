<?php
  final class Database {
    public $connection;

    public static function instance() : Database {
      static $instance = null;
      if ($instance == null) $instance = new Database();
      return $instance;
    }

    public function connect() {
      $this->connection = new mysqli(
        DB_HOST,
        DB_USERNAME,
        DB_PASSWORD,
        DB_DATABASE
      );

      if($this->connection->connect_errno > 0) {
        http_response_code(500);
        die('ERROR: Unable to connect to database!');
      }
    }

    public function query($query) {
      return $this->connection->query($query);
    }

    public function escape($string) : string {
      return $this->connection->escape_string($string);
    }

    public function disconnect() {
      $this->connection->close();
    }

    public function insert_id() : int {
      $this->connection->insert_id;
    }
  }
