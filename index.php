<?php
  require_once 'config.php';
  require_once 'models/database.php';
  require_once 'models/link.php';

  Database::instance()->connect();

  $error = null;

  $id = $_GET['id'];
  $action = $_GET['action'];

  $url = $_POST['url'];

  if (isset($id) or isset($url)) {
    // Find url
    if(isset($id)) {
      $link = Link::find($id);

      if ($link !== null) {
        switch ($action) {
          case 'redirect':
            header("Location: $link->url");
            break;

          case 'show':
            $links = Link::all();
            include 'views/show.php';
            break;
        }
      } else {
        http_response_code(404);
        die('not found');
      }

    // Create new url
    } elseif (isset($url)) {
      $link = new Link();
      $link->url = $url;
      $link->slug = $_POST['slug'];

      if ($link->save()) {
        $local_link = $link->local_url();

        $url = '';
        $slug = '';

        header("Location: /links/$link->slug");
      } else {
        http_response_code(400);
        if (in_array('url', $link->errors)) $error = 'Invalid URL';
        if (in_array('slug', $link->errors)) $error = 'Invalid or already in use slug';

        $url = $link->url;
        $slug = $link->slug;
      }

      $links = Link::all();
      include 'views/form.php';
    }
  } else {
    $links = Link::all();
    include 'views/form.php';
  }

  Database::instance()->disconnect();
